package ConnectDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// Created new class to not repeating

public class ConnectDB {
    public final String connectionUrl = "jdbc:postgresql://localhost:5432/Train";
    public final String username = "postgres";
    public final String password = "228228";
    public static Connection connection = null;

    public void connect() throws SQLException, ClassNotFoundException{
        Class.forName("org.postgresql.Driver");
        connection = DriverManager.getConnection(connectionUrl,username,password);
    }

    public void close() throws SQLException{
        connection.close();
    }
}
