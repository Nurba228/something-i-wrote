import ConnectDB.ConnectDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// Created class to get access to my database and to changing it

public class Crud {
    public static Connection connection = null;

    public Crud(){
        connection = ConnectDB.connection;
    }

    public static void create() throws SQLException{
        Statement statement = connection.createStatement();
        String sql = "create table employees"+
                "(id int SERIAL primary key"+
                "FullName varchar(50)"+
                "direction varchar(50)"+
                "age int" +
                "cost int);";
        statement.executeUpdate(sql);
        System.out.println("Table created");
    }

    public static void insert() throws SQLException{
        Statement statement = connection.createStatement();
        String sql = "insert into employees (Fullname,direction,age,cost) "+ " values ('Tony Stark','LeadProgrammer',27,5000)," +
                "('Robert Pattinson','LevelDesigner',25,4000),('Abraham Linkoln','UIDesigner',30,4000)";
        statement.executeUpdate(sql);
        System.out.println("Specialists were added");
    }

    public static void listOfEmployees() throws SQLException{
        Statement statement = connection.createStatement();
        String sql = "select * from employees";
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()){
            System.out.println(rs.getString("FullName")+" " + rs.getString("direction")+" "+rs.getInt("age")+" "+rs.getInt("cost"));
        }
        System.out.println("");
    }
}
