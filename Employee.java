public class Employee {
    private String FullName;
    private String direction;
    private int age;
    private int cost;
    public Employee(String fullName, String direction, int age,int cost) {
        FullName = fullName;
        this.direction = direction;
        this.age = age;
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }
}
