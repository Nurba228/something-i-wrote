import ConnectDB.ConnectDB;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException,ClassNotFoundException {
        ConnectDB db = new ConnectDB();
        int totalCost;
        db.connect();
        Crud.create();
        Crud.insert();
        Crud.listOfEmployees();
        db.close();

        // i did it bc to show the capabilities of OOP: classes, inheritance, polymorphism, encapsulation.

        LeadProgrammer leadProgrammer = new LeadProgrammer("Tony Stark","Lead Programmer",27,5000);
        LevelDesigner levelDesigner = new LevelDesigner("Robert Pattinson","LevelDesigner",25,4000);
        UIDesigner uiDesigner = new UIDesigner("Abraham Linkoln","UIDesigner",30,4000) ;
        System.out.println(" ");
        leadProgrammer.Coding();
        levelDesigner.Coding();
        uiDesigner.Coding();
        totalCost = leadProgrammer.getCost()+levelDesigner.getCost()+uiDesigner.getCost();
        System.out.println("Total cost of project is " + totalCost);
        System.out.println("Project finished");
    }
}
